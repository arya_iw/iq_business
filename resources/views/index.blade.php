<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('/equip-master/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('/icon/font-awesome-4.7.0/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('/style.css')}}">
</head>

<body>
    {{--  --}}
    <section class="hero-wrap" style="background-image:url(/image/jonathan-riley-VW8MUbHyxCU-unsplash.jpg);"
        data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row description align-items-center justify-content-center">
                <div class="col-md-2"></div>
                <div class="col-md-8 text-center">
                    <div class="text">
                        <img class=" mb-4" src="{{asset('/image/logo-default-340x322.png')}}" alt="" width="90px"
                            height="95px">
                        <h4 class=" text-white">WE OFFER IDEAS THAT<span class="text-white font-weight-bold">
                                RAISE</span></h4>
                        <h4 class=" text-white"><span class="font-weight-bold" style="color:orange">YOUR BUSINESS
                            </span>ABOVE THE EXPECTED
                        </h4>
                        <p class=" text-white">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur
                            veritatis corporis
                            praesentium sunt ratione placeat saepe consequatur iure fugit nobis? Voluptatibus assumenda
                            at doloremque est, quo ipsam voluptatum qui veritatis!</p>
                        <div class="row mb-4">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-2">
                                <h3 class=" font-weight-bold" style="color:orange">25</h3>
                                <p style="margin-top:-10px;" class="text-white">Years of experience</p>
                            </div>
                            <div class="col-sm-2">
                                <h3 class=" font-weight-bold" style="color:orange">54</h3>
                                <p style="margin-top:-10px;" class=" text-white">Succesfull projects</p>
                            </div>
                            <div class="col-sm-2">
                                <h3 class=" font-weight-bold" style="color:orange">18</h3>
                                <p style="margin-top:-10px;" class=" text-white">Partner Program</p>
                            </div>
                            <div class="col-sm-2">
                                <h3 class=" font-weight-bold" style="color:white">24/7</h3>
                                <p style="margin-top:-10px;" class=" text-white">Technical Support</p>
                            </div>
                            <div class="col-sm-2"></div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4 mb-2">
                                    <a href="" class="btn btn-warning btn-lg pt-3 pb-3 font-weight-bold text-hover">
                                        <i class="fa fa-bar-chart fa-3x font-weight-bold">&ThinSpace;</i>
                                        <p class="float-right" style="font-size:14px;">MARKETING</p><br>
                                        <p class="float-right mb-0" style="margin-top:-25px; font-size:14px;;">RESEARCH
                                        </p>
                                    </a>
                                </div>
                                <div class="col-md-4 mb-2">
                                    <a href="" class="btn btn-warning btn-lg pt-3 pb-3 font-weight-bold text-hover">
                                        <i class="fa fa-briefcase fa-3x font-weight-bold">&ThinSpace;</i>
                                        <p class="float-right" style="font-size:14px;">EDUCATION</p><br>
                                        <p class="float-right mb-0" style="margin-top:-25px; font-size:14px;;">
                                            &TRAINNING</p>
                                    </a>
                                </div>
                                <div class="col-md-4 mb-2">
                                    <a href="" class="btn btn-warning btn-lg pt-3 pb-3 font-weight-bold text-hover">
                                        <i class="fa fa-globe fa-3x font-weight-bold">&ThinSpace;</i>
                                        <p class="float-right" style="font-size:14px;">OUTSOURCING</p><br>
                                        <p class="float-right mb-0" style="margin-top:-25px; font-size:14px;">PROGRAMS
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button class="btn btn-warning btn-warning1 btn-sm mt-4">more services</button>
                            </div>
                        </div>
                        {{--  --}}
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </section>

    <section class="page2" style="background-image:url(/image/bg-image-2.jpg);">
        <div class="container">
            <div class="row pt-5">
                <div class="col-md-1" style=""></div>
                <div class="col-md-10">
                    <h4 class=" text-center">OFFERING HELPFUL BUSINESS</h4>
                    <h4 class=" text-center">TIPS & ADVICE TO HELP</h4>
                    <h4 class=" text-center"><span class=" text-white font-weight-bold">YOUR BUSINESS </span><span
                            class=" font-weight-bold" style="color:orange">START UP</span></h4>
                </div>
                <div class="col-md-1" style=""></div>
            </div>
            <div class="row mt-5 text-white">
                <div class="col-md-4 text-center">
                    <h5 class="text-white font-weight-bold">STRATEGIES</h5>
                    <p class="">The business strategies we offer our clients lay out a roadmap of initiatives required
                        to achieve
                        success in selected
                        spheres of business activities.</p>
                    <br>
                    <p class="font-weight-bold">
                        &HorizontalLine;&HorizontalLine;&HorizontalLine;&HorizontalLine;&HorizontalLine;</p>
                    <a class="read-more" href="">read more</a>
                </div>
                <div class="col-md-4 text-center">
                    <h5 class="text-white font-weight-bold">SOLUTIONS</h5>
                    <p>We see one of our company's main goals in providing proper business solutions that are designed
                        for our customers'
                        successful leadership on the market.</p>
                    <br>
                    <p class="font-weight-bold">
                        &HorizontalLine;&HorizontalLine;&HorizontalLine;&HorizontalLine;&HorizontalLine;</p>
                    <a class="read-more" href="">read more</a>
                </div>
                <div class="col-md-4 text-center">
                    <h5 class="text-white font-weight-bold">RESULTS</h5>
                    <p>Our team is 100% result-oriented and it means that we deliver what you expect of us in all cases.
                        You can rely on us even in the most complex projects.
                    </p>
                    <br>
                    <p class="font-weight-bold">
                        &HorizontalLine;&HorizontalLine;&HorizontalLine;&HorizontalLine;&HorizontalLine;</p>
                    <a class="read-more" href="">read more</a>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-md-1"></div>
                <div class="col-md-10 text-center">
                    <h5><a class="read-more1" href="">TRAINING & DEVELOPMENT - </a>
                        <a class="read-more1" href="">EMPLOYEE HANDBOOKS - </a>
                        <a class="read-more1" href="">LAW COMPLIANCE - </a><br>
                        <a class="read-more1" href="">JOB DESCRIPTIONS - </a>
                        <a class="read-more1" href="">HELP CENTER - </a>
                        <a class="read-more1" href="">RECRUITING - </a>
                        <a class="read-more1" href="">CAREER OPPORTUNITIES - </a><br>
                        <a class="read-more1" href="">PAYROLL ADMINISTRATION - </a>
                        <a class="read-more1" href="">TAX PAYMENT - </a>
                        <a class="read-more1" href="">TIME & ATTENDANCE - </a><br>
                        <a class="read-more1" href="">GENERAL LEDGER INTERFACE</a>
                    </h5>
                    <button class="btn btn-sm btn-warning btn-warning1 mt-3 mb-3">read more</button>
                </div>
                <div class="col-md-1"></div>
            </div>
            {{--  --}}
        </div>
        </div>
    </section>
    <section class="page3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 p-0 m-0">
                    <div class="img-container">
                        <img src="/image/thumbnail-1-683x452.jpg" alt="" width="100%">
                        <div class="cover-overlay">
                            <div class="text-overlay">
                                <h3 class="text-white font-weight-bold">Credit & Debt solutions</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 p-0 m-0">
                    <div class="img-container">
                        <img src="/image/thumbnail-2-683x452.jpg" alt="" width="100%">
                        <div class="cover-overlay">
                            <div class="text-overlay">
                                <h3 class="text-white font-weight-bold">Lawyer Solutions and Support</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 p-0 m-0">
                    <div class="img-container">
                        <img src="/image/thumbnail-3-683x452.jpg" alt="" width="100%">
                        <div class="cover-overlay">
                            <div class="text-overlay">
                                <h3 class="text-white font-weight-bold">Monay saving ideas</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="page4" style="background-color:#f9ba2e;">
        <div class="container-fluid">
            <div class="row page4 align-items-center justify-content-center">
                <div class=" col-md-5 page4-text">
                    <p class="text-white font-weight-bold page4-text-h1">Compensation & employee benefits</p>
                </div>
                <div class=" col-md-5 page4-text">
                    <p class="text-white font-weight-bold page4-text-h4">WE TEACH YOUR TEAM HOW TO INCREASE YOUR
                        REVENUES & DECREASE EXPENSES IN BUSINESS MANAGEMENT</p>
                </div>
                <div class=" col-md-2 text-white page4-text">
                    <h6 class=" text-white font-weight-bold">&VerticalBar;<a href="" class="page4-text-learn-more">
                            learn more</a></h6>
                </div>
            </div>
        </div>
    </section>
    <section class="page5 pt-5">
        <div class="container pt-4">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center font-weight-bold text-white">Testimonials</h1>
                </div>
            </div>
            <div class="row pt-5">
                <div class="col-md-4 p-0">
                    <div class="img-container">
                        <img src="{{asset('/image/testimonials-01-390x366.jpg')}}" alt="" width="100%">
                        <div class="cover-overlay">
                            <div class="text-overlay">
                                <h3 class="text-white font-weight-bold">Credit & Debt solutions</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 p-0">
                    <div class="img-container">
                        <img src="{{asset('/image/testimonials-02-390x366.jpg')}}" alt="" width="100%">
                        <div class="cover-overlay">
                            <div class=" text-overlay">
                                <h3 class="text-white font-weight-bold">Credit & Debt solutions</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 p-0">
                    <div class="img-container">
                        <img src="{{asset('/image/testimonials-03-390x366.jpg')}} " alt="" width="100%">
                        <div class="cover-overlay">
                            <div class=" text-overlay">
                                <h3 class="text-white font-weight-bold">Credit & Debt solutions</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pb-5">
                <div class="col-md-4 p-0">
                    <div class="img-container">
                        <img src="{{asset('/image/testimonials-04-390x366.jpg')}} " alt="" width="100%">
                        <div class="cover-overlay">
                            <div class=" text-overlay">
                                <h3 class="text-white font-weight-bold">Credit & Debt solutions</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 p-0">
                    <div class="img-container">
                        <img src="{{asset('/image/testimonials-05-390x366.jpg')}} " alt="" width="100%">
                        <div class="cover-overlay">
                            <div class=" text-overlay">
                                <h3 class="text-white font-weight-bold">Credit & Debt solutions</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 p-0">
                    <div class="img-container">
                        <img src="{{asset('/image/testimonials-06-390x366.jpg')}} " alt="" width="100%">
                        <div class="cover-overlay">
                            <div class=" text-overlay">
                                <h3 class="text-white font-weight-bold">Credit & Debt solutions</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--  --}}
        </div>
    </section>
    <section class="page6">
        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d439724.109296698!2d114.02018887733757!3d30.567815993549477!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x342eaef8dd85f26f%3A0x39c2c9ac6c582210!2sWuhan%2C%20Hubei%2C%20China!5e0!3m2!1sen!2sid!4v1581298256185!5m2!1sen!2sid"
            width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </section>
    <section class="page7 hero-wrap" style="background-image:url(/image/jonathan-riley-VW8MUbHyxCU-unsplash.jpg);">
        <div class="overlay"></div>
        <div class="container text-white font-weight-bold">
            <div class="row pt-5">
                <div class=" col-md-12">
                    <h2 class=" text-center font-weight-bold text-white pb-3">Contact Us</h2>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input class="form-control page7-form" type="text" name="name" id="name" placeholder="Name">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input class="form-control page7-form" type="text" name="email" id="email" placeholder="Email">
                    </div>
                </div>
            </div>
            <div class="row pb-5">
                <div class="col-md-12">
                    <div class="form-group">
                        <textarea id="Message" class="form-control page7-form" name="Message"
                            style="min-height: 400px; color:white">Message</textarea>
                    </div>
                </div>
            </div>
            {{--  --}}
        </div>
    </section>
    <section class="page8">
        <div class="container">
            <div class="row pt-5 pb-5">
                <div class="col-md-3">
                    <img src="{{asset('/image/logo-inverse-340x332.png')}}" alt="" width="140px" height="145px">
                    <p class="page8-p pt-1">&COPY; 2020. All Rights Reserved</p>
                </div>
                <div class="col-md-3">
                    <p>Call us:</p>
                    <h5 class="page8-h">800-2345-6789</h5>
                    <p class="page8-p">IQ Business, 4578 Marmora Road,Glasgow D04 89GR</p>
                    <p class="page8-p" style="margin-top:-20px;">Email: info@demolink.org</p>
                </div>
                <div class="col-md-3">
                    <i class="page8-icon fa fa-facebook pr-2"></i>
                    <i class="page8-icon fa fa-google-plus pr-2"></i>
                    <i class="page8-icon fa fa-linkedin pr-2"></i>
                    <i class="page8-icon fa fa-twitter"></i>
                </div>
            </div>
        </div>
    </section>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
</body>

</html>
